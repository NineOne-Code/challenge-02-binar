// import library
let readline = require('readline');
let rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

// class main
class Main {
  constructor(value) {
    this.value = value
  }
  
  // input user
  input() {
    rl.question('Masukkan Nilai Siswa: ', res => {
      if(res == 'q') {
        this.execute()
        rl.close()
      } else if (isNaN(Math.floor(res)) || res < 0 || res > 100) {
        console.log('Invalid Value')
        rl.close()
      } else {
        this.value.push(res)
        console.log(this.value)
        this.input()
      }
    })
  }

  // cari total semua value
  sum() {
    let total = 0
    for (let i = 0; i < this.value.length; i++) {
      total += +this.value[i]
    }
    return total
  }

  // eksekusi
  execute() {
    const kkm = 75
    const res = this.sum()

    let pass = 0, notPass = 0

    for (let i = 0; i < this.value.length; i++) {
      if (this.value[i] >= kkm) {
        pass += 1
      } else {
        notPass += 1
      }
    }
    console.log(`=====================================================`)
    console.log(`Nilai Tertinggi          : ${Math.max(...this.value)}`)
    console.log(`Nilai Terendah           : ${Math.min(...this.value)}`)
    console.log(`Nilai Rata-rata          : ${(res / this.value.length)}`)
    console.log(`Jumlah Siswa Lulus       : ${pass}`)
    console.log(`Jumlah Siswa Tidak Lulus : ${notPass}`)
    console.log(`=====================================================`)
  }
}


// run program
const obj = new Main([]);
obj.input();